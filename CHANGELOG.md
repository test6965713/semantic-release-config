## [1.1.10](https://gitlab.com/test6965713/semantic-release-config/compare/v1.1.9...v1.1.10) (2023-03-30)


### Bug Fixes

* add maven plugin ([](https://gitlab.com/test6965713/semantic-release-config/commit/756ef5e))

## [1.1.9](https://gitlab.com/test6965713/semantic-release-config/compare/v1.1.8...v1.1.9) (2023-03-30)


### Bug Fixes

* remove maven plugin ([](https://gitlab.com/test6965713/semantic-release-config/commit/b9ab2c9))

## [1.1.8](https://gitlab.com/test6965713/semantic-release-config/compare/v1.1.7...v1.1.8) (2023-03-30)


### Bug Fixes

* fixing maven plugin ([](https://gitlab.com/test6965713/semantic-release-config/commit/639e98c))

## [1.1.7](https://gitlab.com/test6965713/semantic-release-config/compare/v1.1.6...v1.1.7) (2023-03-30)


### Bug Fixes

* fixing maven plugin ([](https://gitlab.com/test6965713/semantic-release-config/commit/c8b29e9))

## [1.1.6](https://gitlab.com/test6965713/semantic-release-config/compare/v1.1.5...v1.1.6) (2023-03-30)


### Bug Fixes

* fixing maven plugin ([](https://gitlab.com/test6965713/semantic-release-config/commit/61d483f))

## [1.1.5](https://gitlab.com/test6965713/semantic-release-config/compare/v1.1.4...v1.1.5) (2023-03-30)


### Bug Fixes

* fixing maven plugin ([](https://gitlab.com/test6965713/semantic-release-config/commit/a84becc))

## [1.1.4](https://gitlab.com/test6965713/semantic-release-config/compare/v1.1.3...v1.1.4) (2023-03-30)


### Bug Fixes

* fixing maven plugin ([](https://gitlab.com/test6965713/semantic-release-config/commit/996226b))

## [1.1.3](https://gitlab.com/test6965713/semantic-release-config/compare/v1.1.2...v1.1.3) (2023-03-30)


### Bug Fixes

* fixing java modules ([](https://gitlab.com/test6965713/semantic-release-config/commit/87b2891))

## [1.1.2](https://gitlab.com/test6965713/semantic-release-config/compare/v1.1.1...v1.1.2) (2023-03-30)


### Bug Fixes

* adding java module ([](https://gitlab.com/test6965713/semantic-release-config/commit/5f45a92))

## [1.1.1](https://gitlab.com/test6965713/semantic-release-config/compare/v1.1.0...v1.1.1) (2023-03-30)


### Bug Fixes

* fixing package-lock ([](https://gitlab.com/test6965713/semantic-release-config/commit/98bdae7))

# [1.1.0](https://gitlab.com/test6965713/semantic-release-config/compare/v1.0.0...v1.1.0) (2023-03-30)


### Features

* adding java config ([](https://gitlab.com/test6965713/semantic-release-config/commit/bb2920d))

# 1.0.0 (2023-03-30)


### Bug Fixes

* add lock ([](https://gitlab.com/test6965713/semantic-release-config/commit/b18ea10))
* Add package lock ([](https://gitlab.com/test6965713/semantic-release-config/commit/8de7525))
