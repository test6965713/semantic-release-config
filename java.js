module.exports = {
    "branches": [
        {
          "name": "release/+([0-9])?(.{+([0-9]),x}).x",
          "range": "${name.replace(/^release\\//g, '')}"
        },
        "main",
        {
            "name": "pre-release/*",
            "prerelease": "rc"
        }
    ],
    "plugins": [
        ["@semantic-release/commit-analyzer", {"preset": "jshint"}],
        ["@semantic-release/release-notes-generator", {"preset": "jshint"}],
        "semantic-release-maven"
        ["@semantic-release/gitlab", {"assets": ["CHANGELOG.md", "setup.py", "setup.cfg"]}],
        ["@semantic-release/git", {"assets": ["CHANGELOG.md"], "message": "[[CHORE]] updating changelog [skip ci] ${nextRelease.notes} \n\n${nextRelease.notes}"}],
    ]
}

